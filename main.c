/*
* File: main.c
* Fieldman OU
* Created on October 4, 2017, 10:09 AM
*/

#pragma config FOSC = INTOSC // Oscillator Selection bits (INTOSC oscillator: CLKIN function disabled)
#pragma config BOREN = OFF // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config WDTE = OFF // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF // MCLR Pin Function Select bit (MCLR pin function is MCLR)
#pragma config CP = OFF // Code Protection bit (Program memory code protection is disabled)
#pragma config LVP = ON // Low-Voltage Programming Enable (Low-voltage programming enabled)
#pragma config LPBOR = OFF // Brown-out Reset Selection bits (BOR enabled)
#pragma config BORV = LO // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config WRT = OFF // Flash Memory Self-Write Protection (Write protection off)

#include <xc.h>
#define _XTAL_FREQ 8000000
#define DEBOUNCE_TIME 10
#define LED_ON_TIME 10
#define LED_NOTIFY_DELAY 400
#define LED_OFF_TIME_SLOW 400
#define LED_OFF_TIME_FAST 100

volatile unsigned char flag;
void delay();
int debounce();
volatile unsigned char sleep;
volatile unsigned char long_short_flag;

void main(void) {

    __delay_ms(500);
    #define _XTAL_FREQ 31000 
    OSCCON &= 0b10001111; // 31 KHZ
    VREGCON |= 0b00000010;
    LATA = 0x00;

    //  PRESCALER = 16
    T2CKPS1 = 1;   
    T2CKPS0 = 0;

    // Initialize flag variable
    flag = 0;
    ANSELA = 0x00;
    TRISA &= 0b11111101;
    #asm // clear on-change condition because RA1 = 1 set it 
    MOVLW 0xff
    XORWF IOCAF, W
    ANDWF IOCAF, F
    #endasm

    // PRESCALER 1 : 4  possible reduction
    PSA = 1;
    PS2 = 0;
    PS1 = 0;
    PS0 = 1;
    T0CS = 0;
    // TMR0IE = 1;
    IOCAP2 = 1; // possible relocation
    GIE = 1;
    PEIE = 1;

    // Initialization indicates off time when active
    long_short_flag = 0;
    while(1)
    {
        if(!flag)
        {
            TMR0IE = 1; // only interrupt allowed is TIMER 0 interrupt;
            RA1 = 1;
            __delay_ms(LED_ON_TIME);
            RA1 = 0;
            if(long_short_flag)
            __delay_ms(LED_OFF_TIME_FAST);
            else
            __delay_ms(LED_OFF_TIME_SLOW);
        }
        else
        {
            RA1 = 0;
            OSCCON = (OSCCON & 0b10001111) | 0b00010000;
            GIE = 0;
            TMR0IE = 0; // TIMER0 interrupt cannot happen
            IOCIE = 1;
            SLEEP();
            OSCCON &= 0b10001111; // 31 KHZ
            debounce();
            IOCIE = 0;
            flag ^= 0b00000001;
            TMR0IE = 1;
            GIE = 1;

    #asm // clear on-change condition because RA1 = 1 set it 
    MOVLW 0xff
    XORWF IOCAF, W
    ANDWF IOCAF, F
    #endasm 

            } 
    }
    return;
}

void interrupt ISR() {
       short int TMR2flag = 0;
       short int T2F = 0;
        if(TMR0IF)
        { 

            if(RA2 != 0)
            {
                delay();
                if(RA2 == 1)
                {
                    RA1 = 1;
                    TMR2 = 0;

                    TMR2ON = 1;
                    TMR2IE = 0;
                    while(RA2 != 0)
                    {   
                        if(TMR2IF == 1)
                        {
                            TMR2flag++;
                            if(TMR2flag == 2) // 2 x ~0.5s: number of overflows, 1 second delay on long press
                            {
                                RA1 = 0;
                                __delay_ms(LED_NOTIFY_DELAY);
                                RA1 = 1;
                            }
                            if(TMR2flag > 1)
                            T2F = 1;
                            TMR2IF = 0;
                         }  
                    }
                    if(T2F)
                    {
                        long_short_flag ^= 1;
                        flag ^= 0b00000001;
                    }   

                    TMR2ON = 0;

                    flag ^= 0b00000001;

                    } 
            }
    TMR0IF = 0;
    IOCAF &= 0x00;
    #asm // clear on-change condition because RA1 = 1 set it 
    MOVLW 0xff
    XORWF IOCAF, W
    ANDWF IOCAF, F
    #endasm 
        }
    }
    void delay()
    {

    TMR2 = 0;
    TMR2ON = 1;

    while(TMR2 < 10);
    TMR2ON = 0;
    }

    int debounce()
    {
    __delay_ms(DEBOUNCE_TIME);

    if(RA2 == 1)
        RA1 = 1;
    while(RA2 ==1);

}